import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '../../services/storage.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { fiscalCodeValidator } from '../../directives/fiscal-code-validator.directive';
import { AuthService } from '../../auth/auth.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Browser } from '@capacitor/browser';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  token: string;
  comuni: any[];
  loading = true;
  error: any;
  profileForm: FormGroup;
  isSubmitted: boolean = false;
  items = [];
  showText: boolean = false;
  private userData: any;
  private unsubscribe$ = new Subject<void>();
  private newDataUser: any;

  constructor(
    private router: Router,
    private storage: StorageService,
    public alertController: AlertController,
    public formBuilder: FormBuilder,
    public modalController: ModalController,
    private authService: AuthService,
    public loadingController: LoadingController
  ) {
    this.profileForm = this.formBuilder.group({
      first_name: [
        {
          value: null,
          disabled: true,
        },
        [Validators.required],
      ],
      last_name: [
        {
          value: null,
          disabled: true,
        },
        [Validators.required],
      ],
      phone: ['', [Validators.required]],
      email: [
        {
          value: null,
          disabled: true,
        },
        ,
        [Validators.required],
      ],
      // municipality: ['', [Validators.required]],
      // address: ['', [Validators.required]],
      fiscal_code: [
        {
          value: null,
          disabled: true,
        },
        [
          Validators.required,
          fiscalCodeValidator(/^[A-Za-z]{6}[0-9]{2}[A-Za-z]{1}[0-9]{2}[A-Za-z]{1}[0-9]{3}[A-Za-z]{1}$/),
        ],
      ],
    });
  }

  get errorControl() {
    return this.profileForm.controls;
  }

  ngOnInit() {
    this.storage.getToken().then((res) => {
      if (res) {
        this.token = JSON.parse(res).token;
      } else {
        this.presentAlertConfirm();
      }
    });

    this.storage.getUserData().then((res) => {
      if (res) {
        this.userData = JSON.parse(res);
        this.populateFormUser(this.userData);
      }
    });

    this.authService
      .getCurrentUser()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.newDataUser = {
          email: res.email,
          first_name: res.first_name,
          fiscal_code: res.fiscal_code,
          last_name: res.last_name,
          phone: res.phone,
        };
        let data_user = {
          phone: res.phone,
          first_name: res.first_name,
          last_name: res.last_name,
        };
        this.profileForm.patchValue(data_user);
      });
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Non sei abilitato a questa sezione',
      message: 'Per accedere al profilo devi essere <strong>registrato</strong> <br> effettua il login e riprova',
      buttons: [
        {
          text: 'Chiudi',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.router.navigate(['home']);
          },
        },
        {
          text: 'Login',
          handler: () => {
            this.router.navigate(['login']);
          },
        },
      ],
    });

    alert.onDidDismiss().then((data) => {
      this.showText = true;
    });

    await alert.present();
  }

  ionViewWillEnter() {
    this.storage.getToken().then((res) => {
      if (res) {
        this.token = JSON.parse(res).token;
      } else {
        this.presentAlertConfirm();
      }
    });

    this.authService
      .getCurrentUser()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.newDataUser = {
          email: res.email,
          first_name: res.first_name,
          fiscal_code: res.fiscal_code,
          last_name: res.last_name,
          phone: res.phone,
        };
        let data_user = {
          phone: res.phone,
          first_name: res.first_name,
          last_name: res.last_name,
        };
        this.profileForm.patchValue(data_user);
      });
  }

  logout() {
    this.removeToken().then(() => {
      this.router.navigate(['home']);
    });
  }

  async removeToken() {
    await this.storage.removeToken();
  }

  async onSubmit() {
    this.isSubmitted = true;
    if (!this.profileForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      const loading = await this.loadingController.create({
        cssClass: 'my-custom-class',
        message: 'Aggiornamento in corso...',
      });
      this.newDataUser.phone = this.profileForm.value.phone;
      await loading.present().then(() => {
        this.authService
          .updateUserProfile(this.newDataUser)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            (res) => {
              this.presentAlertPost('Dati aggiornati con successo', '');
              this.router.navigate(['home']);
            },
            (error) => {
              loading.dismiss();
              this.presentAlertPost('Opss!', 'Ci sono stati dei problemi, riprova più tardi!');
            },
            () => {
              loading.dismiss();
            }
          );
      });
    }
  }

  populateFormUser(user: any) {
    if (this.userData) {
      let data_user = {
        full_name: this.userData.data_user.name,
        phone: this.userData.data_user.phone_number,
        fiscal_code: this.userData.data_user.fiscal_code,
        email: this.userData.data_user.email,
      };
      this.profileForm.patchValue(data_user);
    }
  }

  async presentAlertPost(header: string, message: string) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['Chiudi'],
    });

    await alert.present();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  openPage() {
    Browser.open({ url: 'https://segnalazioni.amiu.genova.it/user/register' });
  }
}
