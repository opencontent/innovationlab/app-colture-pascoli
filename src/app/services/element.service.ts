import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../environments/environment';
import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ElementService {

  constructor(private httpClient: HttpClient) { }

  getElement(municipalityName: string): Observable<any> {
    return this.httpClient.get(`${environment.json_server}/CIVICI?comune=${municipalityName}`).pipe(tap(async (res: any) => {}));
  }
}
